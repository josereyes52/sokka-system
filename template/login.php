<!DOCTYPE html>
<html>
<head>
	<title>login</title>
	<meta name="viewport" content="width=device-width, initial-escale=1.0"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link href="framework/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="framework/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href=" framework/css/login.css">
	
</head>
<body>
	<div class="login-logo">
		<p class="logo-sokka">Sokka</p>
		<small class="text-muted">| System</small>
	</div>

	<div class="jumbotron">
		
		<label>Nombre de usuario</label>
		<div class="use">
			<input type="text" name="username" id="username" class="form-control" placeholder="Enter Username">
			<span class="glyphicon glyphicon-user"></span>
		</div>
		<label>Contrase&#241;a</label>
		<div class="pass">
			<input type="password" name="password" id="password" class="form-control" placeholder="Enter password">
			<span class="glyphicon glyphicon-lock"></span>
		</div>
		<button class="btn btn-success" id="bt">Acceder</button>

		<div class="alert alert-warning" id="alert">
			<strong>Warning!</strong> User And Password Incorrect!
		</div>
	</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<script  src="framework/js/jquery.js"></script>
<script type="text/javascript" src="framework/js/jquery_login.js" ></script>
<!-- <script href="jquery-1.3.2.min.js" type="text/javascript"></script> 
<script href="https://code.jquery.com/jquery-latest.js"></script> -->
<script src="framework/js/bootstrap.min.js"></script>
<!-- <script href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
</body>
</html>