<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Punto De Ventas">
    <meta name="author" content="Jose Reyes">

    <title>Sokka|User Register</title>

    <!-- Bootstrap Core CSS -->
    <link href="framework/css/bootstrap.min.css" rel="stylesheet">
    <!-- style to category register -->
    <link  href="framework/css/category_register.css" rel="stylesheet">
    <!-- <link async="async" rel="stylesheet" type="text/css" href=" ../framework/css/user_registe.css"> -->

    <!-- Custom CSS -->
    <link  href="framework/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link  href="framework/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <?php
    session_start();
    if (empty($_SESSION['login_user'])) {
        header('Location:login');
    }
    ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="_header">
            
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Crear Categorias
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="homepage">Dashboard</a>
                            </li>
                            <li class="active" >
                                <i class="fa fa-edit"></i> Categorias y Sub-Categorias
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="submenu">
                    <ul class="nav nav-tabs">
                        <li id="categoria"><a href="#">Categoria</a></li>
                        <li id="sub_categoria"><a href="#">Sub-Categoria</a></li>
                    </ul>
                    <div class="sub_men_cat"> 
                            
                        
                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-2 col-form-label" id="text_category">Categoria</label>
                            <div class="col-xs-10">
                                <input class="form-control" type="text" id="input_category">
                                <button type="button" class="btn btn-success" id="btn_category">Crear</button>
                            </div>
                        </div>
                    </div>
                    <div class="sub_men_subcat">

                        <div class="option_category">
                            <label class="selec_categ">Categoria</label>
                            <select class="form-control" id="selection">
                                <!-- <option value="null">Null</option> -->
                                
                            </select>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-2 col-form-label" id="text_sub_category">Sub-Categoria</label>
                            <div class="col-xs-10">
                                <input class="form-control" type="text" id="input_sub_category">
                                <button type="button" class="btn btn-success" id="crt_cat">Crear</button>
                            </div>
                        </div>
                    </div>
                    

                </div>  

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script  src="framework/js/jquery.js"></script>
    <script  type="text/javascript" src="framework/js/category_register.js" ></script>


    <!-- Bootstrap Core JavaScript -->
    <script src="framework/js/bootstrap.min.js"></script>

</body>

</html>
