<span class="glyphicon glyphicon-user" id="icono_user"></span>
<div class="inputdata" id="nombre">
    <input type="text" name="name" id="name" class="form-control" placeholder="Enter FirstName">
</div>
<div class="inputdata" id="apellido">
    <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Enter LastName">
</div>
<div class="form-group" id="btdrop"> 
    <span class="tipo_user">Selecciones Tipo De Usuario:</span>
    <select class="form-control" id="type_user">
        <option value="facturacion">Facturacion</option>
        <option value="armacenista">Armacenista</option>
        <option value="administrador">Administrador</option>
    </select>
</div>
<div class="form-group" id="selec_sucursal">
    <span class="selec_sucr">Sucursal:</span>
    <select class="form-control-2">
        <!-- <option value="null">Null</option> -->
        <?php 
        require_once "../module/module.php";
        $requery = new DataBases();
        if($requery->connect()){
            if($row = $requery->sucursal()){
                foreach ($row as $key ) {
                    print_r("<option value = ".$key['id_sucursal'].">".$key['nombre_suc']."</option>");
                }
                $requery->desconect();
            }
        }
        ?>                      
    </select>
</div>  
<div class="inputdata" id="use">
    <input type="text" name="nickname" id="nickname" class="form-control" placeholder="Enter Username">
</div>
<div class="inputdata" id="pass">
    <input type="password" name="password_nick" id="password_nick" class="form-control" placeholder="Enter password">
</div>
    <span class="access-plus">Accesos Adicionales</span>
<div class="funkyradio">
    <div id="aling-left">
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="consul_sup">
                    Consultas De Suplidor 
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="consul_us">
                    Consultas De Usuarios
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="consul_art">
                    Consultas De Articulos 
            </label>
        </div>
    </div>
    <div id="aling-center">
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="reg_sup">
                    Registros De Suplidor
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="reg_comp">
                    Registros Orden De Compras 
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="reg_us">
                    Registros De Usuario
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="reg_art">
                    Registros De Articulos
            </label>
        </div>
    </div>
    <div id="aling-right">
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="invent">
                    Inventario
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="cotiz">
                    Cotizacion
            </label>
        </div>

        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="fact">
                    Facturacion
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input class="check" type="checkbox" value="mant">
                    Mantenimiento
            </label>
        </div>
    </div>
</div>
<button class="btn btn-success" id="btr">Registrar</button>
<div class="alert alert-warning" id="alert">
    <strong>Warning!</strong> Favor verifique los campos en rojo!
</div>
