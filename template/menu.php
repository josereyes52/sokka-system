<!DOCTYPE html>
<html>
<head>
	<title>Sokka|System</title>
	<link rel="stylesheet" type="text/css" href=" ../framework/css/menu.css">
	<link href="../framework/css/bootstrap.min.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="nav-side-menu">
		<div class="brand">Sokka Menu</div>
    	<i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
    	<div class="menu-list">
      		<ul id="menu-content" class="menu-content collapse out">
        		<li>
		          <a href="#">
		            <i class="glyphicon glyphicon-list-alt"></i> Facturar
		          </a>
        		</li>
                <a href="#"><i class="fa fa-globe fa-lg"></i> Articulos <span class="arrow"></span></a>
                    </li>
                    <ul class="sub-menu collapse" id="service">
                      <li>Articulos</li>
                      <li>Categoria</li>
                      <li>Marcas</li>
                      <li>inventario</li>
                    </ul>
		        <li data-toggle="collapse" data-target="#service" class="collapsed">
		          <a href="#"><i class="fa fa-globe fa-lg"></i> Servicios De Consultas <span class="arrow"></span></a>
		        </li>
		        <ul class="sub-menu collapse" id="service">
		          <li>Clientes</li>
		          <li>Suplidores</li>
		          <li>Usuarios</li>
		        </ul>
		        <li data-toggle="collapse" data-target="#new" class="collapsed">
		          <a href="#"><i class="glyphicon glyphicon-registration-mark"></i> Registros <span class="arrow"></span></a>
		        </li>
		        <ul class="sub-menu collapse" id="new">
		          <li>Clientes</li>
		          <li>Suplidor</li>
		          <li>Articulos</li>
		          <a href="/punto%20ventas/template/_user_register.php"><li>Usuarios</li></a>
		        </ul>
		        <li>
		          <a href="#">
		          <i class="glyphicon glyphicon-file"></i> Cotizacion
		          </a>
		        </li>
		        <li>
		          <a href="#">
		          <i class="glyphicon glyphicon-check"></i> Inventario 
		          </a>
		        </li>
		    </ul>
    	</div>
  	</div>
	<div class="user_login">
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user"></span>
                    <strong>Usuario</strong>
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <div class="navbar-login">
                            <div class="row">
                                <div class="col-lg-4">
                                    <p class="text-center">
                                        <span class="glyphicon glyphicon-user icon-size"></span>
                                    </p>
                                </div>
                                <div class="col-lg-8">
                                    <p class="text-left">
                                    <strong>
                                    <?php
                                    session_start();
                                    if ($_SESSION['login_user']) {
                                        printf($_SESSION['login_user']." ".$_SESSION['login_lastname']);
                                    }else{
                                        header('Location:login.php');
                                    }
                                    ?>
                                    </strong>
                                    </p>
                                    <p class="text-left small"><?php printf($_SESSION['sucursal']); ?></p>
                                    <p class="text-left">
                                        <a href="#" class="btn btn-primary btn-block btn-sm">Actualizar Datos</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <div class="navbar-login navbar-login-session">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>
                                        <a href="../module/logout.php" class="btn btn-danger btn-block">Cerrar Sesion</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  	 <script type="text/javascript" src="../framework/js/bootstrap.js" ></script>
  	<script type="text/javascript" src="../framework/js/bootstrap.min.js" ></script>
  	<!-- <script type="text/javascript" src="../framework/js/npm.js" ></script> -->
</body>
</html>