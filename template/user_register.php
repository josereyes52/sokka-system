<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Punto De Ventas">
    <meta name="author" content="Jose Reyes">

    <title>Sokka|User Register</title>

    <!-- Bootstrap Core CSS -->
    <link async="async" href="/sokka-system/framework/css/bootstrap.min.css" rel="stylesheet">
    <link async="async" rel="stylesheet" type="text/css" href="/sokka-system/framework/css/user_registe.css">

    <!-- Custom CSS -->
    <link async="async" href="/sokka-system/framework/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link async="async" href="/sokka-system/framework/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="_header">
            <!-- Brand and toggle get grouped for better mobile display -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            User Register
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="homepage">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> User Register
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="jumbotron">
                    <span class="glyphicon glyphicon-user" id="icono_user"></span>
                    <div class="inputdata" id="nombre">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Enter FirstName">
                    </div>
                    <div class="inputdata" id="apellido">
                        <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Enter LastName">
                    </div>
                    <div class="form-group" id="btdrop"> 
                        <span class="tipo_user">Selecciones Tipo De Usuario:</span>
                        <select class="form-control" id="type_user">
                            <option value="facturacion">Facturacion</option>
                            <option value="armacenista">Armacenista</option>
                            <option value="administrador">Administrador</option>
                        </select>
                    </div>
                    <div class="form-group" id="selec_sucursal">
                        <span class="selec_sucr">Sucursal:</span>
                        <select class="form-control-2">
                            <!-- <option value="null">Null</option> -->
                            <?php 
                            require_once "module/module.php";
                            
                            $requery = new DataBases();
                            if($requery->connect()){
                                if($row = $requery->sucursal()){
                                    foreach ($row as $key ) {
                                        print_r("<option value = ".$key['id_sucursal'].">".$key['nombre_suc']."</option>");
                                    }
                                    $requery->desconect();
                                }
                            }
                            ?>                      
                        </select>
                    </div>  
                    <div class="inputdata" id="use">
                        <input type="text" name="nickname" id="nickname" class="form-control" placeholder="Enter Username">
                    </div>
                    <div class="inputdata" id="pass">
                        <input type="password" name="password_nick" id="password_nick" class="form-control" placeholder="Enter password">
                    </div>
                        <span class="access-plus">Permisos Adicionales</span>
                        <span class="leer">Leer  <span class="edit"> Edit </span> <span class="elim"> Elim  </span>
                    <div class="funkyradio">
                        <div id="aling-left">
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="css">
                                        Consultas De Suplidor 
                                </label>
                                <div class="lect">
                                    <input class="css" id="check_leer" type="checkbox" value="leer">
                                    <input class="css" id="check_edit" type="checkbox" value="edit">
                                    <input class="css" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="csu">
                                        Consultas De Usuarios
                                </label>
                                <div class="lect">
                                        <input class="csu" id="check_leer" type="checkbox" value="leer">
                                        <input class="csu" id="check_edit" type="checkbox" value="edit">
                                        <input class="csu" id="check_elim" type="checkbox" value="eliminar">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="csa">
                                        Consultas De Articulos 
                                </label>
                                <div class="lect">
                                    <input class="csa" id="check_leer" type="checkbox" value="leer">
                                    <input class="csa" id="check_edit" type="checkbox" value="edit">
                                    <input class="csa" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>
                        </div>
                        <div id="aling-center">
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="rgs">
                                        Registros De Suplidor
                                </label>
                                <div class="lect">
                                    <input class="rgs" id="check_leer" type="checkbox" value="leer">
                                    <input class="rgs" id="check_edit" type="checkbox" value="edit">
                                    <input class="rgs" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="rgc">
                                        Registros Orden De Compras 
                                </label>
                                <div class="lect">
                                    <input class="rgc" id="check_leer" type="checkbox" value="leer">
                                    <input class="rgc" id="check_edit" type="checkbox" value="edit">
                                    <input class="rgc" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="rgu">
                                        Registros De Usuario
                                </label>
                                <div class="lect">
                                    <input class="rgu" id="check_leer" type="checkbox" value="leer">
                                    <input class="rgu" id="check_edit" type="checkbox" value="edit">
                                    <input class="rgu" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="rga">
                                        Registros De Articulos
                                </label>
                                <div class="lect">
                                    <input class="rga" id="check_leer" type="checkbox" value="leer">
                                    <input class="rga" id="check_edit" type="checkbox" value="edit">
                                    <input class="rga" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>
                        </div>
                        <div id="aling-right">
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="inv">
                                        Inventario
                                </label>
                                <div class="lect">
                                    <input class="inv" id="check_leer" type="checkbox" value="leer">
                                    <input class="inv" id="check_edit" type="checkbox" value="edit">
                                    <input class="inv" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="cot">
                                        Cotizacion
                                </label>
                                <div class="lect">
                                    <input class="cot" id="check_leer" type="checkbox" value="leer">
                                    <input class="cot" id="check_edit" type="checkbox" value="edit">
                                    <input class="cot" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="fact">
                                        Facturacion
                                </label>
                                <div class="lect">
                                    <input class="fact" id="check_leer" type="checkbox" value="leer">
                                    <input class="fact" id="check_edit" type="checkbox" value="edit">
                                    <input class="fact" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input class="check" type="checkbox" value="mant">
                                        Mantenimiento
                                </label>
                                <div class="lect">
                                    <input class="mant" id="check_leer" type="checkbox" value="leer">
                                    <input class="mant" id="check_edit" type="checkbox" value="edit">
                                    <input class="mant" id="check_elim" type="checkbox" value="eliminar">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success" id="btr">Registrar</button>
                    <div class="alert alert-warning" id="alert">
                        <strong>Warning!</strong> Favor verifique los campos en rojo!
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script  src="framework/js/jquery.js"></script>
    <script  type="text/javascript" src="framework/js/jquery_register.js" ></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="framework/js/bootstrap.min.js"></script>

</body>

</html>
