<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
     <link href="/sokka-system/framework/css/bootstrap.min.css" rel="stylesheet">
     <link href="/sokka-system/framework/css/mant_usuario.css" rel="stylesheet">
     <link href="/sokka-system/framework/css/sb-admin.css" rel="stylesheet">
     <link href="/sokka-system/framework/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <title></title>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="_header">
                <!-- Brand and toggle get grouped for better mobile display -->
        </nav>
        <div class="col-lg-12">
            <h1 class="page-header">
                Consulta De Suplidor
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>  <a href="homepage">Dashboard</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Consulta
                </li>
            </ol>
        </div> 
        <div id="contenido" >
            <div class="panel panel-primary">
                <div class="panel-body">
                <div class="search_div">
                    <label>Buscar</label>
                    <input type="text" name="search" id="txtbuscar" class="form-control" placeholder="RNC o Nombre">
                </div>
                    <table class="table table-striped">
                        <thead>
                            <!-- <th>#</th> -->
                            <th>ID</th>
                            <th>Suplidor</th>
                            <th>Represent</th>
                            <th>Direccion</th>
                            <th>Email</th>
                            <th>Telefono</th>
                            <th>Movil Represent</th>
                            <th>RNC</th>
                            <!-- <th>Pagina Web</th> -->
                            <th>Retardo</th>
                            <th>Comentario</th>
                            <!-- <th>Fecha De Creacion</th>
                            <th>Creado Por</th> -->
                            <th>Editar</th>  
                            <th>Eliminar</th>           
                        </thead>
                        <tbody>
                            <?php
                            require_once "module/module.php";
                            $requery = new DataBases();
                            if($requery->connect()){
                                $query = 'SELECT * FROM suplidor';
                                if($rows = $requery->query($query)){
                                    if ($rows == 'Sin Datos') {
                                        echo $rows;
                                    }else if (count($rows) == 1) {
                                        echo "<tr>";
                                        echo '<td>' . $row['id_suplidor'] . '</td>';
                                        echo '<td>' . $row['suplidor'] . '</td>';
                                        echo '<td>' . $row['representante'] . '</td>';
                                        echo '<td>' . $row['direccion'] . '</td>';
                                        echo '<td>' . $row['email'] . '</td>';
                                        echo '<td>' . $row['telefono'] . '</td>';
                                        echo '<td>' . $row['movil_rept'] . '</td>';
                                        echo '<td>' . $row['rnc'] . '</td>';
                                        // echo '<td>' . $row['pag_web'] . '</td>';
                                        echo '<td>' . $row['tiem_pedido'] . '</td>';
                                        echo '<td>' . $row['comentario'] . '</td>';
                                        // echo '<td>' . $row['imf_date'] . '</td>';
                                        // echo '<td>' . $row['creado_por'] . '</td>';
                                        echo '<td>'.'<button type="button" class="btn btn-primary btn-sm" id="editar">'."Editar".'</button>'.'</td>';
                                        echo '<td>'.'<button type="button" class="btn btn-primary btn-sm" id = "eliminar">'."Eliminar".'</button>'.'</td>';
                                    }else{
                                        foreach ( $rows as $row) {
                                            // var_dump($row['id_suplidor']);
                                            echo "<tr>";
                                            echo '<td>' . $row['id_suplidor'] . '</td>';
                                            echo '<td>' . $row['suplidor'] . '</td>';
                                            echo '<td>' . $row['representante'] . '</td>';
                                            echo '<td>' . $row['direccion'] . '</td>';
                                            echo '<td>' . $row['email'] . '</td>';
                                            echo '<td>' . $row['telefono'] . '</td>';
                                            echo '<td>' . $row['movil_rept'] . '</td>';
                                            echo '<td>' . $row['rnc'] . '</td>';
                                            // echo '<td>' . $row['pag_web'] . '</td>';
                                            echo '<td>' . $row['tiem_pedido'] . '</td>';
                                            echo '<td>' . $row['comentario'] . '</td>';
                                            // echo '<td>' . $row['imf_date'] . '</td>';
                                            // echo '<td>' . $row['creado_por'] . '</td>';
                                            echo '<td>'.'<button type="button" class="btn btn-primary btn-sm" id="editar">'."Editar".'</button>'.'</td>';
                                            echo '<td>'.'<button type="button" class="btn btn-primary btn-sm" id = "eliminar">'."Eliminar".'</button>'.'</td>';
                                        }
                                    $requery->desconect();
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script  src="framework/js/jquery.js"></script>
    <script src = "framework/js/mant_suplidor.js"></script>
    <script src="framework/js/bootstrap.min.js"></script>
</body>
</html>