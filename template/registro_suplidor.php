<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Punto De Ventas">
    <meta name="author" content="Jose Reyes">

    <title>Sokka| Registro De Suplidores</title>

    <!-- Bootstrap Core CSS -->
    <link href="framework/css/bootstrap.min.css" rel="stylesheet">
    <!-- style to category register -->
    <link  href="framework/css/registro_suplidor.css" rel="stylesheet">
    <!-- <link async="async" rel="stylesheet" type="text/css" href=" ../framework/css/user_registe.css"> -->

    <!-- Custom CSS -->
    <link  href="framework/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link  href="framework/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <?php
    session_start();
    if (empty($_SESSION['login_user'])) {
        header('Location:login');
    }
    ?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="_header">
            
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Registro De Suplidores
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="homepage">Dashboard</a>
                            </li>
                            <li class="active" >
                                <i class="fa fa-edit"></i> Rgt Suplidores
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <form class="form-horizontal">

                    <!-- Text input-->
                    <div class="form-group" id="content-suplidor">
                        <label class="col-md-4 control-label" for="Nombre">Suplidor</label>
                        <div class="col-md-5" id="input-suplidor">
                            <input id="suplidor" name="Nombre" type="text" placeholder="Suplidor" class="form-control input-md" required="">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group" id="content-representante">
                        <label class="col-md-4 control-label" for="Empresa">Representante</label>
                        <div class="col-md-5" id="input-representante">
                            <input id="representante" name="Empresa" type="text" placeholder="Representate" class="form-control input-md" required="">

                        </div>
                    </div>

                    <div class="form-group" id="content-direccion">
                        <label class="col-md-4 control-label" for="Empresa">Direccion</label>
                        <div class="col-md-5" id="input-direccion">
                            <input id="direccion" name="Empresa" type="text" placeholder="Direccion" class="form-control input-md" required="">

                        </div>
                    </div>

                    <div class="form-group" id="content-email">
                        <label class="col-md-4 control-label" for="Empresa">Email</label>
                        <div class="col-md-5" id="input-email">
                            <input id="email" name="Empresa" type="email" placeholder="@Email" class="form-control input-md" required="">

                        </div>
                    </div>

                    <div class="form-group" id="content-tel">
                        <label class="col-md-4 control-label" for="Empresa">Tel:</label>
                        <div class="col-md-5" id="input-email">
                            <input id="tel" name="Empresa" type="text" placeholder="Telefono" class="form-control input-md" required="">

                        </div>
                    </div>

                    <div class="form-group" id="content-movil">
                        <label class="col-md-4 control-label" for="Empresa">Movil Reprensentante</label>
                        <div class="col-md-5" id="input-movil">
                            <input id="movil_rep" name="Empresa" type="text" placeholder="Movil" class="form-control input-md" required="">

                        </div>
                    </div>

                    <div class="form-group" id="content-rnc">
                        <label class="col-md-4 control-label" for="Empresa">RNC</label>
                        <div class="col-md-5" id="input-rnc">
                            <input id="rnc" name="Empresa" type="text" placeholder="Rnc" class="form-control input-md" required="">

                        </div>
                    </div>

                    <div class="form-group" id="content-pagweb">
                        <label class="col-md-4 control-label" for="Empresa">Pagina Web</label>
                        <div class="col-md-5" id="input-pagweb">
                            <input id="pag_web" name="Empresa" type="text" placeholder="Pag web" class="form-control input-md" required="">

                        </div>
                    </div>

                   <!--  <div class="form-group" id="content-limitcred">
                        <label class="col-md-4 control-label" for="Empresa">Limite De Credito</label>
                        <div class="col-md-5" id="input-limitcred">
                            <input id="lim_cred" name="Empresa" type="text" placeholder="Limite" class="form-control input-md" required="">

                        </div>
                    </div> -->

                   <!--  <div class="form-group" id="content-balance">
                        <label class="col-md-4 control-label" for="Empresa">Balance</label>
                        <div class="col-md-5" id="input-balance">
                            <input id="balance" name="Empresa" type="text" placeholder="Balance" class="form-control input-md" required="">

                        </div>
                    </div -->

                    <div class="form-group" id="content-tiempo-retardo">
                        <label class="col-md-4 control-label" for="Empresa">Tiempo De Retardo</label>
                        <div class="col-md-5" id="input-tiempo-retardo">
                            <input id="time_ret" name="Empresa" type="text" placeholder="Tiempo retardo" class="form-control input-md" required="">

                        </div>
                    </div>

                    <div class="comment">
                       <textarea placeholder="comentarios" class="box-coment"></textarea>
                    </div>
                </form>
                <div class="alert alert-warning" id="alert">
                        <strong>Warning!</strong> Favor verifique los campos en rojo!
                </div>
                <button class="btn btn-success" id="btr">Registrar</button>
            </div>
            <!-- /.container-fluid -->


        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!-- jQuery -->
<script  src="framework/js/jquery.js"></script>
<script  type="text/javascript" src="framework/js/jquery_rgt_suplidor.js" ></script>


<!-- Bootstrap Core JavaScript -->
<script src="framework/js/bootstrap.min.js"></script>

</body>

</html>