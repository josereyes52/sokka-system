<!DOCTYPE html>
<html>
<head>
	<title>Registro De Usuarios</title>
	<meta name="viewport" content="width=device-width, initial-escale=1, user-scalable=no"/>
	<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> -->
	<link rel="stylesheet" type="text/css" href="../framework/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href=" ../framework/css/menu.css">
	<link rel="stylesheet" type="text/css" href=" ../framework/css/user_registe.css">
</head>
<body>
		<div class="nav-side-menu">
			<div class="brand">Sokka Menu</div>
			<i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
			<div class="menu-list">
				<ul id="menu-content" class="menu-content collapse out">
					<li>
						<a href="#">
							<i class="glyphicon glyphicon-list-alt"></i> Facturar
						</a>
					</li>
					<li data-toggle="collapse" data-target="#service" class="collapsed">
						<a href="#"><i class="fa fa-globe fa-lg"></i> Articulos <span class="arrow"></span></a>
					</li>
					<ul class="sub-menu collapse" id="service">
						<li>Articulos</li>
						<li>Categoria</li>
						<li>Marcas</li>
						<li>inventario</li>
					</ul>
					<li data-toggle="collapse" data-target="#service" class="collapsed">
						<a href="#"><i class="fa fa-globe fa-lg"></i> Servicios De Consultas <span class="arrow"></span></a>
					</li>
					<ul class="sub-menu collapse" id="service">
						<li>Clientes</li>
						<li>Suplidores</li>
						<li>Usuarios</li>
					</ul>
					<li data-toggle="collapse" data-target="#new" class="collapsed">
						<a href="#"><i class="glyphicon glyphicon-registration-mark"></i> Registros <span class="arrow"></span></a>
					</li>
						<ul class="sub-menu collapse" id="new">
							<li>Clientes</li>
							<li>Suplidor</li>
							<li>Articulos</li>
							<li><a href="/punto%20ventas/template/_user_register.php">Usuarios</a></li>
						</ul>
					<li>
						<a href="#">
							<i class="glyphicon glyphicon-file"></i> Cotizacion
						</a>
					</li>
					<li>
						<a href="#">
							<i class="glyphicon glyphicon-check"></i> Inventario
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="user_login">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="glyphicon glyphicon-user"></span>
						<strong>Usuario</strong>
						<span class="glyphicon glyphicon-chevron-down"></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<div class="navbar-login">
								<div class="row">
									<div class="col-lg-4">
										<p class="text-center">
											<span class="glyphicon glyphicon-user icon-size"></span>
										</p>
									</div>
									<div class="col-lg-8">
										<p class="text-left">
											<strong>
												<?php
												session_start();
												if ($_SESSION['login_user']) {
													printf($_SESSION['login_user']." ".$_SESSION['login_lastname']);
												}else{
													header('Location:login.php');
												}
												?>
											</strong>
										</p>
										<p class="text-left small"><?php printf($_SESSION['sucursal']); ?></p>
										<p class="text-left">
											<a href="#" class="btn btn-primary btn-block btn-sm">Actualizar Datos</a>
										</p>
									</div>
								</div>
							</div>
						</li>
						<li class="divider"></li>
						<li>
							<div class="navbar-login navbar-login-session">
								<div class="row">
									<div class="col-lg-12">
										<p>
											<a href="../module/logout.php" class="btn btn-danger btn-block">Cerrar Sesion</a>
										</p>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="formulario">
			<div class="login-logo">
				<p class="logo-sokka">Sokka</p>
				<small class="text-muted">| Registro</small>
			</div>
			<div class="jumbotron">
				<span class="glyphicon glyphicon-user" id="icono_user"></span>
				<div class="inputdata" id="nombre">
					<input type="text" name="name" id="name" class="form-control" placeholder="Enter FirstName">
				</div>
				<div class="inputdata" id="apellido">
					<input type="text" name="lastname" id="lastname" class="form-control" placeholder="Enter LastName">
				</div>
				<div class="form-group" id="btdrop"> 
					<span class="tipo_user">Selecciones Tipo De Usuario:</span>
					<select class="form-control">
						<option value="facturacion">Facturacion</option>
						<option value="armacenista">Armacenista</option>
						<option value="administrador">Administrador</option>
					</select>
				</div>
				<div class="form-group" id="selec_sucursal">
					<span class="selec_sucr">Sucursal:</span>
					<select class="form-control-2">
						<option value="null">Null</option>
						<?php 
						require_once "../module/module.php";
						$requery = new DataBases();
						if($requery->connect()){
							if($row = $requery->sucursal()){
								foreach ($row as $key ) {
									print_r("<option value = ".$key['id_sucursal'].">".$key['nombre_suc']."</option>");
								}
								$requery->desconect();
							}
						}
						?>						
					</select>
				</div>	
				<div class="inputdata" id="use">
					<input type="text" name="nickname" id="nickname" class="form-control" placeholder="Enter Username">
				</div>
				<div class="inputdata" id="pass">
					<input type="password" name="password_nick" id="password_nick" class="form-control" placeholder="Enter password">
				</div>
					<span class="access-plus">Accesos Adicionales</span>
				<div class="funkyradio">
					<div class="funkyradio-default">
						<input type="checkbox" name="checkbox" id="checkbox1" />
						<label for="checkbox1">Mantenimiento</label>
					</div>
					<div class="funkyradio-primary">
						<input type="checkbox" name="checkbox" id="checkbox2"/>
						<label for="checkbox2">Consulta</label>
					</div>
					<div class="funkyradio-success">
						<input type="checkbox" name="checkbox" id="checkbox3"/>
						<label for="checkbox3">Todos Los Registros</label>
					</div>
					<div class="funkyradio-danger">
						<input type="checkbox" name="checkbox" id="checkbox4"/>
						<label for="checkbox4">Inventario</label>
					</div>
					<div class="funkyradio-warning">
						<input type="checkbox" name="checkbox" id="checkbox5"/>
						<label for="checkbox5">Cotizacion</label>
					</div>
					<div class="funkyradio-info">
						<input type="checkbox" name="checkbox" id="checkbox6"/>
						<label for="checkbox6">Facturacion</label>
					</div>
				</div>
				<button class="btn btn-success" id="btr">Registrar</button>
				<div class="alert alert-warning" id="alert">
					<strong>Warning!</strong> Favor verifique los campos en rojo!
				</div>
			</div>
		</div>
<script href="https://code.jquery.com/jquery-latest.js" ></script>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"  ></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" ></script>
<script href="jquery-1.3.2.min.js" type="text/javascript" ></script>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" >
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script> 
<script type="text/javascript" src="../framework/js/bootstrap.js" ></script>
<script type="text/javascript" src="../framework/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="../framework/js/jquery_register.js" ></script>
</body>
</html>
<!-- for password $re = "/^\S*(?=\S{6,})(?=\S*[a-z])(?=\S*[\d])\S*$/"; 
$str = ""; 
 
preg_match($re, $str, $matches); -->


<!-- regular for email
$re = "/^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,3})$/"; 
$str = "asfagasgasrfasfagagasgas@asfasfgas.com"; 
 
preg_match($re, $str, $matches);
 -->

 <!-- for lastname and name
$re = "/^[\\p{L}\\s-]+$/"; 
$str = ""; 
 
preg_match($re, $str, $matches);
  -->

  <!-- nickname
$re = "/^[\\p{L}\\s-._+#(=?\\s*\\d)]+$/"; 
$str = "jose-asasasdasf_#*+."; 
 
preg_match($re, $str, $matches);
   -->