<!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Sokka|System</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
            <ul class="dropdown-menu message-dropdown">
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                            <!-- <span class="pull-left">
                                <img class="media-object" src="http://placehold.it/50x50" alt="">
                            </span> -->
                            <div class="media-body">
                                <h5 class="media-heading"><strong>
                                    <?php
                                    // print_r($_GET['link'].'neka');
                                    
                                    session_start(); 
                                    if ($_SESSION['login_user']) {
                                        printf($_SESSION['login_user']." ".$_SESSION['login_lastname']);
                                    }else{
                                        header('Location:/sokka-system/template/login.php');
                                    }
                                    ?>
                                </strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                            <!-- <span class="pull-left">
                                <img class="media-object" src="http://placehold.it/50x50" alt="">
                            </span> -->
                            <div class="media-body">
                                <h5 class="media-heading"><strong>
                                    <?php
                                    if ($_SESSION['login_user']) {
                                        printf($_SESSION['login_user']." ".$_SESSION['login_lastname']);
                                    }else{
                                        header('Location:login.php');
                                    }
                                    ?>
                                </strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                            <!-- <span class="pull-left">
                                <img class="media-object" src="http://placehold.it/50x50" alt="">
                            </span> -->
                            <div class="media-body">
                                <h5 class="media-heading"><strong>
                                    <?php
                                    if ($_SESSION['login_user']) {
                                        printf($_SESSION['login_user']." ".$_SESSION['login_lastname']);
                                    }else{
                                        header('Location:login.php');
                                    }
                                    ?>
                                </strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-footer">
                    <a href="#">Read All New Messages</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">View All</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
            <?php
            if ($_SESSION['login_user']) {
                printf($_SESSION['login_user']." ".$_SESSION['login_lastname']);
            }else{
                header('Location:login.php');
            }
            ?>
            <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="active">
                <a href="homepage"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="charts.html"><i class="fa fa-fw fa-bar-chart-o"></i> Charts</a>
            </li>
            <li>
                <a href="tables.html"><i class="fa fa-fw fa-table"></i> Tables</a>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#registro"><i class="fa fa-fw fa-edit"></i> Registro <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="registro" class="collapse">
                    <li>
                        <a href="user_register">Rgt Usuario</a>
                    </li>
                    <li>
                        <a href="registro_articulos">Articulos</a>
                    </li>
                    <li>
                        <a href="registro_ordcompra">Rgt Orden De Compras</a>
                    </li>
                    <li>
                        <a href="registro_suplidor">Rgt Suplidor</a>
                    </li>
                    <li>
                        <a href="category_register">Rgt Categorias</a>
                    </li>
                    <li>
                        <a href="agregar_permisos">Permisos</a>
                    </li>
                </ul>                        
            </li>



            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#mantenimiento"><i class="fa fa-fw fa-wrench"></i> Mantenimientos <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="mantenimiento" class="collapse">
                    <li>
                        <a href="mant_usuario">Usuario</a>
                    </li>
                    <li>
                        <a href="#">Articulos</a>
                    </li>
                    <li>
                        <a href="#">Orden De Compras</a>
                    </li>
                    <li>
                        <a href="mant_suplidor">Suplidor</a>
                    </li>
                    <li>
                        <a href="mant_categoria">Categorias</a>
                    </li>
                    <li>
                        <a href="#">Permisos</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="bootstrap-elements.html"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a>
            </li>
            <li>
                <a href="bootstrap-grid.html"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo" class="collapse">
                    <li>
                        <a href="#">Dropdown Item</a>
                    </li>
                    <li>
                        <a href="#">Dropdown Item</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="inventario"><i class="fa fa-fw fa-file"></i> Inventario</a>
            </li>
            <li>
                <a href="index-rtl.html"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->