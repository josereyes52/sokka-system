<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
     <link href="/sokka-system/framework/css/bootstrap.min.css" rel="stylesheet">
     <link href="/sokka-system/framework/css/mant_usuario.css" rel="stylesheet">
     <link href="/sokka-system/framework/css/sb-admin.css" rel="stylesheet">
     <link href="/sokka-system/framework/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <title></title>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="_header">
                <!-- Brand and toggle get grouped for better mobile display -->
        </nav>
        <div class="col-lg-12">
            <h1 class="page-header">
                Consulta De Categoria
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>  <a href="homepage">Dashboard</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Categorias
                </li>
            </ol>
        </div> 
        <div id="contenido" >
            <div class="panel panel-primary">
                <div class="panel-body">
                <div class="search_div">
                    <label>Buscar</label>
                    <input type="text" name="search" id="txtbuscar" class="form-control" placeholder="ID o Nombre De Categoria">
                </div>
                    <table class="table table-striped">
                        <thead>
                            <!-- <th>#</th> -->
                            <th>ID</th>
                            <th>Categoria</th>
                            <th>Editar</th>           
                        </thead>
                        <tbody>
                            <?php
                            require_once "module/module.php";
                            $requery = new DataBases();
                            if($requery->connect()){
                                $query = 'SELECT * FROM categoria';
                                if($rows = $requery->query($query)){
                                    if ($rows == 'Sin Datos') {
                                        echo $rows;
                                    }else if (count($rows) == 1) {
                                        echo "<tr>";
                                        echo '<td>' . $row['id_categoria'] . '</td>';
                                        echo '<td>' . $row['categoria'] . '</td>';
                                        echo '<td>'.'<button type="button" class="btn btn-primary btn-sm" id="editar">'."Editar".'</button>'.'</td>';
                                        }else{
                                        foreach ( $rows as $row) {
                                            echo "<tr>";
                                            echo '<td>' . $row['id_categoria'] . '</td>';
                                            echo '<td>' . $row['categoria'] . '</td>';
                                            echo '<td>'.'<button type="button" class="btn btn-primary btn-sm" id="editar">'."Editar".'</button>'.'</td>';
                                        }
                                    $requery->desconect();
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script  src="framework/js/jquery.js"></script>
    <script src = "framework/js/mant_categoria.js"></script>
    <script src="framework/js/bootstrap.min.js"></script>
</body>
</html>