<div class="option_category">
    <label class="selec_categ">Categoria</label>
    <select class="form-control" id="selection">
        <!-- <option value="null">Null</option> -->
        <?php 
            require_once "../module/module.php";
            
            $requery = new DataBases();
            if($requery->connect()){
                if($row = $requery->categoria()){
                    foreach ($row as $key ) {
                        print_r("<option value = ".$key['id_categoria'].">".$key['categoria']."</option>");
                    }
                    $requery->desconect();
                }
            }
        ?>                      
    </select>
</div>

<div class="form-group row">
    <label for="example-text-input" class="col-xs-2 col-form-label" id="text_sub_category">Sub-Categoria</label>
    <div class="col-xs-10">
        <input class="form-control" type="text" id="input_sub_category">
        <button type="button" class="btn btn-success" id="crt_cat">Crear</button>
    </div>
</div>