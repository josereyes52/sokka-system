<!-- This link would go in your header tag -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/sokka-system/framework/css/404.css">

<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 text-center">
      <br>
      <h4>OoOoOops The site is down  :'(</h4>
      <p><img src="https://cdn3.iconfinder.com/data/icons/tango-icon-library/48/camera-photo-128.png" alt=""></p>

      <h2><i class="fa fa-exclamation-triangle" style="color:red"></i>
     
 Page not found <small>404 error</small></h2>
      <p>Well, this is embarrassing.

        <br>
      </p>

      <p><a href= "/sokka-system/homepage" >Click here</a> to visit our home page</p>

    </div>

  </div>


 <!--  <p>Best viewed <a href="http://bootsnipp.com/iframe/OeKdM" target="_blank">full screen</a></p>
  <p>
    <a href="http://validator.w3.org/check?uri=http%3A%2F%2Fbootsnipp.com%2Fiframe%2FOeKdM" class="pull-left" target="_blank">
      <small>HTML</small><sup>5</sup>
    </a>
  </p> -->

</div>