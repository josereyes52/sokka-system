$(document).ready(function(){
	$('#bt').click(function(){
		access();
	});

	$('#password').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			access();
		}
	})

	function access(){
		dict_value = {'username':$('#username').val(),
					  'password':$('#password').val()};
		$.ajax({
		    type : "POST",
		    url  : "module/login_verification.php",
		    data : dict_value,
		    success:function(response){
				console.log(response);
				if (response == 'true') {
					document.location.href = 'homepage';

				}else{
		    		$("#alert").css("visibility", "visible");
					$('#password').val('');
		    	}  	
		    },
		    beforeSend:function(){
		    	// console.log("antes");
		    }
		    
		});
	}
});

