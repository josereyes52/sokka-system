$(document).ready(function() {
	regex = /^[a-z0-9_\\_\ü]+$/;
	var cache_table ;
	$("#_header").load("template/_header.php");
	// search = '';
	$("#txtbuscar").keyup(function(e){
		if (e.key =='Enter' && regex.test($(this).val()) || $(this).val()=='') {
			$.ajax({
			type:"POST",
			url:"/sokka-system/module/search_user.php",
			data:{data:$(this).val()},
			success:function(response){
					$("tbody").html("");
					$("tbody").append(response);
				}
			});
		}else if (e.key == 'Backspace') {
			$(this).val("");
		}
	});

	$(this).on('click','#eliminar', function(){
		id = $(this).parents("tr").find("td").eq(0).html();
		db = 'login';
		$(this).closest('tr').remove();
		$.ajax({
		    type : "POST",
		    url  : "/sokka-system/module/edit_user.php",
		    data : {id: id, db:db, id_campo:'id_login'},
		    success: function(response){
				if (response == 'true') {
					var tmpl = '<div class="alerta alert-success alert-dismissable">'+'<button class="close" data-dismiss="alert">&times;</button>'+'Success'+'</div>';
					$('#wrapper').append(tmpl);
					setTimeout(function(){
						$('.alerta').addClass('on');
					},100);
					setTimeout(function(){
						$('.alerta').addClass('off');
					},1900);

				}else{
					alert('Elemento no encontrado');

				}
		    },
		    beforeSend:function(){
				// pass
		    }
		});

	});
});
