$(document).ready(function(){
	//regular expresion for user register
	$("#_header").load("template/_header.php");
	var reg_name = /^[a-zA-Z\s]+$/;
	var reg_nick = /^[a-zA-Z\d-_+#.]+$/;
	var reg_pass = /^\S*(?=\S{6,})([a-zA-Z\d\S])+$/;
	//value_dict @variable for count and save the user data validated
	var value_dict = {name:0,
					  lastname:0,
					  nickname:0,
					  password_nick:0};
	//access_plus @variable for save all the permissions in the system
	access_plus=[];
	permisos = Object();
		// "css":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"csu":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"csa":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"rgs":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"rgc":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"rgu":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"rga":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"inv":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"cot":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"fact":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''},
		// 		"mant":{'leer':'',
		// 			   'edit':'',
		// 			   'eliminar':''}
				/*};*/
	disable_check();
	$('.lect').children(this).click(function(){
		name_class = this.className;
		if (permisos[name_class]===undefined) {
			// console.log("creado");
			permisos[this.className] = {'leer':0,'edit':0,'eliminar':0};

		}if ($(this).is(":checked")) {
			// console.log("correxto")
			lectura = $(this).val();
			permisos[this.className][lectura] = 1;
				console.log(permisos);
		}else{
			lectura = $(this).val();
			// console.log(lectura)
			permisos[this.className][lectura] = 0;

			// console.log(permisos)
		}
	});
	$('.check').click(function(){
				if ($(this).is(":checked")) {
					if (jQuery.isArray(access_plus)) {
						access_plus.push($(this).val());
						disable_check()
					}else{
						access_plus = [];
						access_plus.push($(this).val());
						disable_check()
					}
				}else{
					var item = $(this).val();
					delete permisos[item];
					// permisos = new Object();
					// console.log('class'+item);
					$("."+item).prop("checked", false);
					access_plus.splice($.inArray(item, access_plus),1);
					disable_check()
				}
			});

	$("#name").keyup(function(){
		if(validation_RegExp(reg_name, $(this).val()) == 'true'){
			color_validate('#name','true');
			validation('name','true');
		}else{
			color_validate('#name','false');
			validation('name','false');
		}
	});

	$("#lastname").keyup(function(){
		if(validation_RegExp(reg_name, $(this).val()) == 'true'){
			color_validate('#lastname','true');
			validation('lastname','true');
		}else{
			color_validate('#lastname','false');
			validation('lastname','false');
		}
	});

	$("#nickname").keyup(function(){
		if(validation_RegExp(reg_nick, $(this).val()) == 'true'){
			color_validate('#nickname','true');
			validation('nickname','true');
		}else{
			color_validate('#nickname','false');
			validation('nickname','false');
		}
	});

	$("#password_nick").keyup(function(){
		if(validation_RegExp(reg_pass, $(this).val()) == 'true'){
			color_validate('#password_nick','true');
			validation('password_nick','true');
		}else{
			color_validate('#password_nick','false');
			validation('password_nick','false');
		}
	});

	function validation_RegExp(reg_epress,value){
		if(reg_epress.test(value)){
			return 'true';
		}else{
			return 'false';
		}
	}
	function color_validate(ident,value){
		if(value == 'true'){
			$(ident).css('border','2px solid rgba(15, 163, 18, 0.69)');
		}else{
			$(ident).css('border','2px solid #e90d0d');
		}
	}

	$("#btr").click(function(){
		value = 0
		for (var val in value_dict) {
			if (value_dict[val] == 0){
				$("#alert").css('visibility','visible');
				color_validate('#'+ val, 'false');
				value += 1;
			}
		}
		if (value == 0 ){
			if (access_plus.length == 0){
				access_plus = 'NULL';
			}

			dict_values = {
							'username':$('#nickname').val(),
						   'password':$('#password_nick').val(),
						   'name':$('#name').val(),
						   'lastname':$('#lastname').val(),
						   'id_sucursal':$('.form-control-2').val(),
						   'access_plus':access_plus,
						   'type_user':$('#type_user').val(),
						   'permiso':permisos
						};
			$.ajax({
			    type : "POST",
			    url  : "/sokka-system/module/user_regiter.php",
			    data : dict_values,
			    async: true,
			    success: function(response){
					console.log(response)
					if (response == 'true') {
			    		var tmpl = '<div class="alerta alert-success alert-dismissable">'+'<button class="close" data-dismiss="alert">&times;</button>'+'Usuario Registrado'+'</div>';
						$('#wrapper').append(tmpl);
						setTimeout(function(){
							$('.alerta').addClass('on');
						},100);
						setTimeout(function(){
							$('.alerta').addClass('off');
						},1900);
						setTimeout(function(){
			    			$(".jumbotron").load('template/jumbotron_register.php');
						},1000);
					}else{
						$("#alert").css("visibility", "visible");
					}
			    },
			    beforeSend:function(){
					console.log("antes");
			    }
			});
		}
	});

	function validation(id,value){
		if (value == "true"){
			value_dict[id]=1;
		}else{
			value_dict[id]=0;
		}
	}
	function disable_check(){
		class_check = ["css","csu","csa","rgs","rgc","rgu","rga","inv","cot","fact","mant"];
		if(jQuery.isEmptyObject(access_plus)){
			for (var i in class_check ) {
				$("."+class_check[i]).prop({"disabled": true});
			}
		}else{
			for (var e in access_plus){
				$("."+access_plus[e]).prop({"disabled": false});
			}
		}
	}
});
