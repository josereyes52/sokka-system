$(document).ready(function() {
	// inserting html menu header
	$("#_header").load("template/_header.php");

	// regular expretion to validate the data
	var reg_name = /^[a-zA-Z\s]+$/;
	var reg_email = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/;
	var reg_cel = /^(\+\d{1,3}[- ]?)?\d{10}$/;
	var reg_url = /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
	var reg_rnc = /^[0-9]+([0-9]*)?$/;

	//value_dict @variable for count and save the user data validated
	var value_dict = {suplidor:0,
					  representante:0,
					  email:0,
					  tel:0,
					  movil_rep:0,
					  pag_web:0,
					  rnc:0};

	$("#suplidor").keyup(function(){
		if(validation_RegExp(reg_name, $(this).val()) == 'true'){
			color_validate('#suplidor','true');
			validation('suplidor','true');
		}else{
			color_validate('#suplidor','false');
			validation('suplidor','false');
		}
	});

	$("#representante").keyup(function(){
		if(validation_RegExp(reg_name, $(this).val()) == 'true'){
			color_validate('#representante','true');
			validation('representante','true');
		}else{
			color_validate('#representante','false');
			validation('representante','false');
		}
	});

	$("#email").keyup(function(){
		if(validation_RegExp(reg_email, $(this).val()) == 'true'){
			color_validate('#email','true');
			validation('email','true');
		}else{
			color_validate('#email','false');
			validation('email','false');
		}
	});

	$("#tel").keyup(function(){
		if(validation_RegExp(reg_cel, $(this).val()) == 'true'){
			color_validate('#tel','true');
			validation('tel','true');
		}else{
			color_validate('#tel','false');
			validation('tel','false');
		}
	});

	$("#rnc").keyup(function(){
		if(validation_RegExp(reg_rnc, $(this).val()) == 'true'){
			color_validate('#rnc','true');
			validation('rnc','true');
		}else{
			color_validate('#rnc','false');
			validation('rnc','false');
		}
	});

	$("#movil_rep").keyup(function(){
		if(validation_RegExp(reg_cel, $(this).val()) == 'true'){
			color_validate('#movil_rep','true');
			validation('movil_rep','true');
		}else{
			color_validate('#movil_rep','false');
			validation('movil_rep','false');
		}
	});

	$("#pag_web").keyup(function(){
		if(validation_RegExp(reg_url, $(this).val()) == 'true'){
			color_validate('#pag_web','true');
			validation('pag_web','true');
		}else{
			color_validate('#pag_web','false');
			validation('pag_web','false');
		}
	});

	$("#btr").click(function(){
		value = 0
		for (var val in value_dict) {
			if (value_dict[val] == 0){
				$("#alert").css('visibility','visible');
				color_validate('#'+ val, 'false');
				value += 1;
			}
		}
		if (value == 0 ){
			dict_values = {
							   'suplidor':$('#suplidor').val(),
							   'representante':$('#representante').val(),
							   'direccion':$('#direccion').val(),
							   'email':$('#email').val(),
							   'tel':$('#tel').val(),
							   'movil_rep':$('#movil_rep').val(),
							   'rnc':$('#rnc').val(),
							   'pag_web':$('#pag_web').val(),
							   'lim_cred':$('#lim_cred').val(),
							   'balance':$('#balance').val(),
							   'time_ret':$('#time_ret').val(),
							   'coment':$('.box-coment').val()
							};

			$.ajax({
			    type : "POST",
			    url  : "/sokka-system/module/register_suplidor.php",
			    data : dict_values,
			    async: true,
			    success: function(response){
					if (response == 'true') {
						var tmpl = '<div class="alerta alert-success alert-dismissable">'+'<button class="close" data-dismiss="alert">&times;</button>'+'Usuario Registrado'+'</div>';
						$('#wrapper').append(tmpl);
						setTimeout(function(){
							$('.alerta').addClass('on');
						},100);
						setTimeout(function(){
							$('.alerta').addClass('off');
						},1900);

						$('#suplidor').val('');
						$('#representante').val('');
						$('#direccion').val('');
						$('#email').val('');
						$('#tel').val('');
						$('#movil_rep').val('');
						$('#rnc').val('');
						$('#pag_web').val('');
						$('#lim_cred').val('');
						$('#balance').val('');
						$('#time_ret').val('');
						$('.box-coment').val('');
						for (var name in value_dict ) {
							value_dict[name] = 0;
							color_validate('#' + name, 'defaul');
						}

					}else{
						$("#alert").css("visibility", "visible");

					}
			    },
			    beforeSend:function(){
					// pass
			    }
			});
		}
	})

	function validation_RegExp(reg_epress,value){
		if(reg_epress.test(value)){
			return 'true';
		}else{
			return 'false';
		}
	}

	function color_validate(ident,value){

		if(value == 'true'){
			$(ident).css('border','2px solid rgba(15, 163, 18, 0.69)');
		}else{
			if( value == 'defaul'){
				$(ident).css('border','1px solid #ccc');
			}else{
				$(ident).css('border','2px solid #e90d0d');
			}
		}
	}

	function validation(id,value){
		if (value == "true"){
			value_dict[id]=1;
		}else{
			value_dict[id]=0;
		}
	}
});