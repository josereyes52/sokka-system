$(document).ready(function() {
	var reg_name = /^[a-zA-Z\s]+$/;
	$("#_header").load("template/_header.php");

	$("#categoria").addClass("active");

	$(".sub_men_subcat").css('display','none');
	$("#sub_categoria").click(function(){
		$('#selection').load('/sokka-system/module/load_category.php');
		$('.sub_men_cat').css('display','none');
		$(".sub_men_subcat").css('display','block');
		$(".active").removeClass("active");
		$("#sub_categoria").addClass("active");
		
	})

	$("#categoria").click(function(){
		console.log('cat');
		$(".sub_men_subcat").css('display','none');
		$('.sub_men_cat').css('display','block');
		$(".active").removeClass("active");
		$("#categoria").addClass("active");
		
	})

	$("#crt_cat").click(function(){
		dict_values = {
			           'id_categoria':$('#selection').val(),
					   'sub_categoria':$("#input_sub_category").val()

						};
		$.ajax({
			    type : "POST",
			    url  : "/sokka-system/module/register_subcateg.php",
			    data : dict_values,
			    async: true,
			    success: function(response){
			    	console.log(response);
			    	if (response == 'true') {
			    		var tmpl = '<div class="alerta alert-success alert-dismissable">'+'<button class="close" data-dismiss="alert">&times;</button>'+'SubCategoria Registrado'+'</div>';
						$('#wrapper').append(tmpl);
						setTimeout(function(){
							$('.alerta').addClass('on');
						},100);
						setTimeout(function(){
							$('.alerta').addClass('off');
						},1900);
						$("#input_sub_category").val('');
			    	}else{
			    		// console.log(response);
			    		// $("#alert").css("visibility", "visible");
			    	}  	
			    },
			    beforeSend:function(){
			    	console.log("antes");
			    }
			})
	});

	$("#btn_category").click(function(){
		if (reg_name.test($("#input_category").val())) {
			dict_values = {
						   'categoria':$("#input_category").val()
							};
			$.ajax({
				    type : "POST",
				    url  : "/sokka-system/module/register_categ.php",
				    data : dict_values,
				    async: true,
				    success: function(response){
				    	console.log(response);
				    	if (response == 'true') {
				    		var tmpl = '<div class="alerta alert-success alert-dismissable">'+'<button class="close" data-dismiss="alert">&times;</button>'+'Categoria Registrado'+'</div>';
							$('#wrapper').append(tmpl);
							setTimeout(function(){
								$('.alerta').addClass('on');
							},100);
							setTimeout(function(){
								$('.alerta').addClass('off');
							},1900);
							$("#input_category").val('');
							

				    	}else{
				    		// console.log(response);
				    		// $("#alert").css("visibility", "visible");
				    	}  	
				    },
				    beforeSend:function(){
				
				    }
				});

		}else{
			alert('llene los campos necesatios');
		}
	})
});