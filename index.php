<?php
function getCurrentUri(){
    $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0,-1)) . '/';
    $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));

    if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
    $uri = '/' . trim($uri, '/');
    return $uri;
    // print_r($uri);
    // die;
}

$base_url = getCurrentUri();
// printf($base_url);
// die;
if ($base_url == '/' or $base_url == '/homepage') {
    // printf('format');
    include 'template/homepage.php';
}
elseif ($base_url == '/user_register') {
    include 'template/user_register.php';

}
elseif ($base_url =='/logout') {
    include 'module/logout.php';
    # code...
}
elseif ($base_url =='/login') {
    include 'template/login.php';
    # code...
}
elseif ($base_url == '/category_register') {
    include 'template/category_register.php';
    # code...
}
elseif ($base_url == '/agregar_permisos') {
    include 'template/agregar_permisos.php';
    # code...
}
elseif ($base_url == '/registro_suplidor') {
    include 'template/registro_suplidor.php';
    # code...
}
elseif ($base_url == '/registro_ordcompra') {
    include 'template/registro_ordcompra.php';
    # code...
}
elseif ($base_url == '/registro_articulos') {
    include 'template/registro_articulos.php';
    # code...
}
elseif ($base_url == '/mant_usuario'){
    include 'template/mant_usuario.php';
}
elseif ($base_url == '/mant_suplidor'){
    include 'template/mant_suplidor.php';
}
elseif ($base_url == '/mant_categoria'){
    include 'template/mant_categoria.php';
}
// elseif ($base_url =='/_category_register') {
//     include 'template/_category_register.php';
//     # code...
// }
else{
    include 'template/404.php';
}
// printf($base_url);
// $routes = array();
// $routes = explode('/', $base_url);
// foreach ($routes as $route) {
//     if(trim($route) != '')
//         array_push($routes, $route);
// }

// if($routes[0] == 'search'){
//     if ($routes[1]=='book') {
//         searchBooksby($routes[2]);
//     }
// }
// var_dump($routes);


?>
